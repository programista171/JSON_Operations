import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.FileWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Main {

	public static void main(String[] args) throws IOException
{
Main serwer = new Main();
serwer.send("http://localhost");
}

String JSONCreate() throws IOException
{
JSONObject obj = new JSONObject();
		obj.put("FirstName", "Dawid");
		obj.put("LastName", "Kowalski");
        return obj.toString();
	}
int send(String url) throws IOException
{
URL addr = new URL(url);
HttpURLConnection polaczenie = (HttpURLConnection) addr.openConnection();
polaczenie.setRequestMethod("POST");
polaczenie.setRequestProperty("Content-Type", "application/json");
polaczenie.setDoOutput(true);
DataOutputStream output = new DataOutputStream(polaczenie.getOutputStream());
//doklej do rzadania nasze dane
output.writeBytes(JSONCreate());
output.flush();
output.close();
//obsluga odpowiedzi serwera
int responseCode = polaczenie.getResponseCode();
BufferedReader reader = new BufferedReader(new InputStreamReader(polaczenie.getInputStream()));
        String line;
        StringBuilder response = new StringBuilder();
        while((line = reader.readLine()) != null){
            response.append(line);
        }
        reader.close();
        System.out.println("Wys�ano ��danie pod adres: " + url);
        System.out.println("Kod odpowiedzi: " + responseCode);
        System.out.println("Odpowied�:");
        System.out.println(response.toString());
return 0;
}
}